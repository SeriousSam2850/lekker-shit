/**
 * Basic Machine learning example
 * @author Justin
 *
 */
public class model {
	
	//Weights vector  = vector x
	int[] weights;
	
	
	//Set of training data Matrix A plus a column
	public int[][] trainingData;
	
	//Array of answers for training data  = vector b
	public int[] answers;
	
	public model(int[][] trainingInputs, int[] answers) {
		trainingData = new int[trainingInputs.length][trainingInputs[0].length + 1];
		this.answers = answers;
		weights = new int[trainingInputs[0].length + 1];
	}
	
	
	public void setup(int[][] trainingInputs) {
		//Set Up data
		for (int i = 0; i < trainingInputs.length; i++) {
			for (int j = 0; j < trainingInputs[i].length; j++) {
				trainingData[i][j] = trainingInputs[i][j];
			}
		}
		for (int i = 0; i < trainingInputs.length; i++) {
			trainingData[i][trainingInputs[0].length] = 1;
		}
		
		
	}
	
}
