import java.util.ArrayList;

public class Vertex {

	private int vertexNum;
	private ArrayList<Edge> edges;
	
	public Vertex(int vertexNum){
		this.vertexNum = vertexNum;
		edges = new ArrayList<Edge>();
	}
	
	public void addEdge(Vertex to){
		edges.add(new Edge(this, to));
	}
	
	public ArrayList<Edge> getEdges(){
		return edges;
	}
	
	public String getNum(){
		return ""+vertexNum;
	}
	
	public int getNumber(){
		return vertexNum;
	}
	
}
