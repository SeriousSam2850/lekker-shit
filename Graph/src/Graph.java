import java.awt.Color;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Graph {

	public static final int DIRECTED = 0;
	public static final int UNDIRECTED = 1;
	public static final int WEIGHTEDDIRECTED = 2;

	private Vertex[] vertices;
	private Edge[] edges;

	public Graph(String graph, int type){
		File file = new File(graph);
		try {
			Scanner scan = new Scanner(file);

			//Set size pf vertex set
			if(scan.hasNextLine()){
				vertices = new Vertex[Integer.parseInt(scan.nextLine())];
			}
			//Set size or edge set
			if(scan.hasNextLine()){
				if(type == DIRECTED){
					edges = new Edge[Integer.parseInt(scan.nextLine())];
				} else {
					edges = new Edge[2*Integer.parseInt(scan.nextLine())];
				}
				
			}

			//Makes all the vertices
			for(int i = 0; i< vertices.length; i++){
				vertices[i] = new Vertex(i);
			}

			//Connect vertices
			for(int i = 0 ; i< edges.length; i++){
				if(scan.hasNextLine()){
					String line = scan.nextLine();
					String[] nums = line.split(" ");
					int to, from, weight = 0;
					from = Integer.parseInt(nums[0]);
					to = Integer.parseInt(nums[1]);
					if(type == WEIGHTEDDIRECTED){
						weight = Integer.parseInt(nums[2]);
					}
					
					if(type == DIRECTED){
						edges[i]= new Edge(vertices[from], vertices[to]);
					} else if(type ==UNDIRECTED) {
						edges[i++]= new Edge(vertices[from], vertices[to]);
						edges[i]= new Edge( vertices[to], vertices[from]);
					} else if(type == WEIGHTEDDIRECTED) {
						edges[i]= new Edge(vertices[from], vertices[to], weight);
					}
					vertices[from].addEdge(vertices[to]);
				}
			}

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	private void setup(){
		//Init weight matrix
		//Make all null first null = inf
		//Run through edges and place them in a double array 
	}


	public void Draw(){
		StdDraw.setCanvasSize(800, 800);
		StdDraw.setXscale(-400,400);
		StdDraw.setYscale(-400,400);
		StdDraw.setPenColor(Color.black);

		int[][] positions = new int[vertices.length][2];
		for(int[] pos: positions){
			pos[0] = (int)((Math.random()*700)-350);
			pos[1] = (int)((Math.random()*700)-350);
		}



		for(int i = 0; i<edges.length; i++){
			int to = edges[i].getToVertex().getNumber();
			int from = edges[i].getFromVertex().getNumber();
			int[] pos1 = positions[from];
			int[] pos2 = positions[to];

			StdDraw.setPenColor(Color.black);
			StdDraw.line(pos1[0], pos1[1], pos2[0], pos2[1]);

			//Bit of vector work
			float[] dir = {(pos2[0]-pos1[0]), (pos2[1]-pos1[1])};

			double norm = Math.sqrt(dir[0]*dir[0]+dir[1]*dir[1]);

			dir = new float[]{20*((float)(dir[0]/norm)),20*((float)(dir[1]/norm))};
			StdDraw.setPenColor(Color.GREEN);
			StdDraw.filledCircle(pos2[0]-dir[0], pos2[1]-dir[1], 5);


		}

		for(int i =0; i< vertices.length; i++){
			int[] pos = positions[i];
			StdDraw.setPenColor(Color.black);
			StdDraw.filledCircle(pos[0], pos[1], 20);
			StdDraw.setPenColor(Color.WHITE);
			StdDraw.text(pos[0], pos[1], ""+(Integer.parseInt((vertices[i].getNum()))+0));
		}

		StdDraw.show();


	}


	public Vertex[] getVertices() {
		return vertices;
	}


	public void setVertices(Vertex[] vertices) {
		this.vertices = vertices;
	}


	public Edge[] getEdges() {
		return edges;
	}


	public void setEdges(Edge[] edges) {
		this.edges = edges;
	}

}
