
public class Edge {

	private Vertex from, to;
	private int weight;
	
	public Edge(Vertex from, Vertex to){
		this.from = from; 
		this.to = to;
	}
	
	public Edge(Vertex from, Vertex to, int weight){
		this.from = from; 
		this.to = to;
		this.weight = weight;
	}
	
	public Vertex getToVertex(){
		return to;
	}
	public Vertex getFromVertex(){
		return from;
	}
	
}
