package dev;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import Entities.Drone;
import Entities.Maker;
import Entities.Runner;
import dev.input.Input;
import dev.tile.Tile;
import projectiles.Bullet;

public class Maze {

	private Tile[][] grid; 
	private Maker[] makers;
	private Drone d;
	private int size;
	private Runner runner;
	private int count;
	private Input in;

	public Maze(int size){
		makers= new Maker[4];
		makers[0] = new Maker(0,0);
		makers[1] = new Maker(0,size-1);
		makers[2] = new Maker(size-1,0);
		makers[3] = new Maker(size-1,size-1);
		grid = new Tile[size][size];
		this.size =size;

		count = 0;

		for(int i = 0; i<size; i++){
			for (int j = 0; j<size ; j++){
				grid[i][j] = new Tile(i, j);
			}
		}
	}

	public Maze(String f){

		makers= new Maker[4];
		makers[1] = new Maker(0,size-1);
		makers[2] = new Maker(size-1,0);
		makers[3] = new Maker(size-1,size-1);


		File file= new File(f);
		try {
			Scanner scan = new Scanner(file);

			this.size = scan.nextInt();

			grid = new Tile[size][size];
			makers[0] = new Maker(scan.nextInt(), scan.nextInt());

			String line=scan.nextLine();;
			for(int i = 0; i<size; i++){
				if(scan.hasNextLine()) {
					line=scan.nextLine();
					for (int j = 0; j<size ; j++){
						if(line.charAt(j)=='#') {
							grid[i][j] = new Tile(i, j, true);
						} else if(line.charAt(j)=='*') {
							grid[i][j] = new Tile(i, j, false);
						} else {
							grid[i][j] = new Tile(i, j);
						}
					}
				}
			}
			scan.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} 
	}

	public void renderNeighbours(int x , int y) {
		if((x<size&&x>=0) &&(y<size&&y>=0)) {
			Tile[] these = neighbours(x, y);
			for (int i = 0; i<these.length; i++) {
				if(these[i]!= null) {
					these[i].render();
				}
			}
		}
	}
	public void renderWalls(){
		
	}

	public void render(){

		for (int i = 0; i<1; i++){
			if(makers[i]!=null)
				renderNeighbours(makers[i].getX(), makers[i].getY());
		}

		if(runner!=null)
			renderNeighbours(runner.getX(), runner.getY());
		if(d!=null)
			renderNeighbours(d.getX(), d.getY());


		for (int i = 0; i<1; i++){
			if(makers[i] != null)
				makers[i].render();
		}


		if(runner != null) {
			runner.render();
			for(Bullet b: runner.getBbs()) {
				b.render();
				renderNeighbours(b.getX(), b.getY());
			}
		}

		if(d != null) {
			d.render();
		}
	}

	public void renderFull() {
		for(int i =0; i<size; i++) {
			for(int j = 0; j<size; j++) {
				grid[i][j].render();
			}
		}
	}

	public void tick(){
		if(finished()) {
			renderFull();
		}
		if(finished() && runner==null) {
			renderFull();
			count++;
			if(count ==2) {
				for (int i = 0; i<4; i++){
					makers[i]=null;
				}

				runner = new Runner(this);
				d = new Drone(this);
				in = new Input(this, runner);
			}
		}
		//		for(int i = 0; i<4; i++){
		//		if(makers[i] != null)
		//			makers[i].tick(this);
		//		}
		if(in != null)
			in.tick();

		for (int i = 0; i<1; i++){
			if(makers[i] != null)
				makers[i].tick(this);
		}


		if(runner != null) {
			ArrayList<Bullet> toRemove = new ArrayList<Bullet>();
			runner.tick();
			for(Bullet b: runner.getBbs()) {
				if(b.outOfBouds()) {
					toRemove.add(b);
				}
				b.tick();
			}
			ArrayList<Bullet> bb = runner.getBbs();
			bb.removeAll(toRemove);
			runner.setBbs(bb);
		}

		if(d != null) {
			d.tick();
		}
	}

	public boolean finished() {

		if(makers[0]!=null){
			return makers[0].isFinished();
		}else {
			return false;
		}
	}

	public void breakWalls(int xo, int yo, int x, int y){
		//Find direction
		// 1 == left 
		// 2 == right
		// 3 == up
		// 4 == down
		int dir=0;
		if(Math.abs(xo-x)==1){
			if((xo-x)==-1){
				dir = 2;
			} else {
				dir = 1;
			}
		} else if(Math.abs(yo-y)==1){
			if((yo-y)==-1){
				dir = 3;
			} else {
				dir = 4;
			}
		}

		//Break walls in that direction
		if(dir == 1){
			grid[xo][yo].destroyWall(1);
			grid[x][y].destroyWall(3);
		} else if( dir == 2 ){
			grid[xo][yo].destroyWall(3);
			grid[x][y].destroyWall(1);
		} else if( dir == 3 ){
			grid[xo][yo].destroyWall(2);
			grid[x][y].destroyWall(0);
		} else if( dir == 4 ){
			grid[xo][yo].destroyWall(0);
			grid[x][y].destroyWall(2);
		}
	}

	public Tile getTile(int x, int y ){
		return grid[x][y];
	} 

	public Tile[] getNeighbours(int x, int y) {

		Tile[] nieghbours = new Tile[4];

		if(isAvalible(x-1,y)) {
			nieghbours[0]=grid[x-1][y];
		}

		if(isAvalible(x+1,y)) {
			nieghbours[1]=grid[x+1][y];
		}

		if(isAvalible(x,y-1)) {
			nieghbours[2]=grid[x][y-1];
		}

		if(isAvalible(x,y+1)) {
			nieghbours[3]=grid[x][y+1];
		}

		return nieghbours;
	}

	public Tile[] neighbours(int x, int y) {

		Tile[] nieghbours = new Tile[8];

		if(x-1>=0) {
			nieghbours[0]=grid[x-1][y];
		}

		if(x+1<size) {
			nieghbours[1]=grid[x+1][y];
		}

		if(y-1>=0) {
			nieghbours[2]=grid[x][y-1];
		}

		if(y+1<size) {
			nieghbours[3]=grid[x][y+1];
		}
		if(x<size-1&&y<size-1)
			nieghbours[4]=grid[x+1][y+1];
		if(x>0&&y<size-1)
			nieghbours[5]=grid[x-1][y+1];
		if(x>0&&y>0)
			nieghbours[6]=grid[x-1][y-1];
		if(x<size-1&&y>0)
			nieghbours[7]=grid[x+1][y-1];

		return nieghbours;
	}

	private boolean isAvalible(int x, int y) {
		if(x<size && x>=0) {
			if(y<size && y>=0){
				if(!grid[x][y].isVisited()) {return true;} else {return false;}
			}  else {return false;}
		} else {return false;}
	}



}
