package dev;

public class Launcher {

	private static Generator geni;
	
	public static void main(String[] args) {
		geni = new Generator();
		Settings.tileWidth = Settings.width/Integer.parseInt(args[0]);
		if(args.length==2) {
			geni.run(0, args[1]);
		} else {
			geni.run(Integer.parseInt(args[0]), null);
		}
		
	}
	
}
