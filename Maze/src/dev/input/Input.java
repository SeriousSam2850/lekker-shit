package dev.input;

import Entities.Runner;
import dev.Maze;
import dev.gfx.StdDraw;

public class Input {
	
	private Maze m;
	private Runner runner;
	
	public Input(Maze m, Runner runner) {
		this.m = m;
		this.runner = runner;
	}

	public void tick() {
		if(StdDraw.hasNextKeyTyped()) {
			char k = StdDraw.nextKeyTyped();
			//UP
			if((int)(k)==119) {
				runner.move(1);
				
				//Left
			} else if((int)(k)==97) {
				runner.move(2);
				
				//Down
			} else if((int)(k)==115) {
				runner.move(3);
				
				//Right
			} else if((int)(k)==100) {
				runner.move(4);
			} else if((int)(k)==102) {
				runner.shoot();
			}
		}
	}
	
	
}
