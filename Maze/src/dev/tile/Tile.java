package dev.tile;

import java.awt.Color;

import dev.Settings;
import dev.gfx.StdDraw;

public class Tile {

	private boolean[] walls;
	private boolean visited;
	private int x;
	private int y;
	private float xp;
	private float yp;
	private float halfTile;
	private Color color1, color2;

	public Tile(int x, int y){
		walls= new boolean[4];
		visited=false;
		this.x = x; 
		this.y = y;

		color1=new Color(0,191,255);
		color2=new Color(0,255,255);

		xp = x*Settings.tileWidth;
		yp = y*Settings.tileWidth;
		halfTile = Settings.tileWidth/2;

		for(int  i=0; i<4 ; i++){
			walls[i]=true;
		}
	}

	public Tile(int x, int y, boolean b){
		walls= new boolean[4];
		visited=b;
		this.x = x; 
		this.y = y;

		if(b) {
			color1=new Color(255,20,147);
		} else {
			color1=new Color(255,0,255);
		}
		color2=new Color(255,20,147);

		xp = x*Settings.tileWidth;
		yp = y*Settings.tileWidth;
		halfTile = Settings.tileWidth/2;

	}

	public void render(){
		
		StdDraw.filledSquare(xp, yp, halfTile);

		if(visited){
			StdDraw.setPenColor(color1);
			StdDraw.filledRectangle(xp, yp, halfTile+halfTile/50, halfTile+halfTile/50);
		}else {
			StdDraw.setPenColor(color2);
			StdDraw.filledRectangle(xp, yp, halfTile+halfTile/50, halfTile+halfTile/50);

		}
		drawWalls();

	}


	public boolean hasWall(int i) {
		return walls[i];
	}


	/*
	 * 0 == south
	 * 1 == west
	 * 2 == north
	 * 3 == east
	 */

	public void drawWalls(){
		for(int i= 0; i<4; i++){
			StdDraw.setPenColor(StdDraw.BLACK);
			StdDraw.setPenRadius(0.01);
			if(walls[i]){
				if(i==0){
					StdDraw.line((xp-halfTile)+(halfTile/50),yp-halfTile , xp+halfTile-(halfTile/50), yp-halfTile);
				}
				if(i==1){
					StdDraw.line(xp-halfTile,(yp-halfTile)+(halfTile/50) , xp-halfTile, (yp+halfTile)-(halfTile/50));
				}
				if(i==2){
					StdDraw.line((xp-halfTile)+(halfTile/50),yp+halfTile,(xp+halfTile)-(halfTile/50), yp+halfTile);
				}
				if(i==3){
					StdDraw.line(xp+halfTile,(yp+halfTile)-(halfTile/50),xp+halfTile, (yp-halfTile)+(halfTile/50));
				}
			}
		}
	}

	public void destroyWall(int i){
		walls[i]=false;
	}

	public void tick(){

	}

	//Getters and Setters
	public boolean[] getWalls() {
		return walls;
	}
	public void setWalls(boolean[] walls) {
		this.walls = walls;
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}

	public boolean isVisited() {
		return visited;
	}

	public void visit(boolean v) {
		visited = v;
	}


}
