package dev;

import dev.gfx.Screen;
import dev.gfx.StdDraw;

public class Generator {

	private Maze maze;
	private boolean running;
	private Screen screen;


	private void tick(){
		maze.tick();
	}
	
	private void render(){
		maze.render();
		
		//Show and clear the screen
//		screen.show();
		StdDraw.show();
	}

	private void init(int size){
		maze = new Maze(size);
		screen = new Screen(Settings.width);
		running = true;
	}
	
	private void init(String map){
		maze = new Maze(map);
		screen = new Screen(Settings.width);
		running = true;
	}

	//GameLoop
	public void run(int size, String map) {

		if(map != null) {
			init(map);
		} else {
			init(size);
		}
		
		
		render();

		int fps = 9999999;
		double timePerTick = 1000000000 / fps;
		double delta = 0;
		long now;
		long lastTime = System.nanoTime();
		long timer = 0;
		int ticks = 0;

		while (running){
			now = System.nanoTime();
			delta += (now- lastTime) / timePerTick;
			timer += now - lastTime;
			lastTime = now;

			if(delta >= 1) {

				tick();
				render();

				ticks++;
				delta=0;
			}

			if(timer >= 1000000000) {
				System.out.println("FPS = " + ticks);
				ticks = 0;
				timer = 0;
			}
		}
	}

}
