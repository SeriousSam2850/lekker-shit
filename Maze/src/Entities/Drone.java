package Entities;

import java.awt.Color;
import java.util.ArrayList;

import dev.Maze;
import dev.Settings;
import dev.gfx.StdDraw;

public class Drone {

	private int counter;
	private Maze m;

	private int x, y;
	private float xp, yp, halfTile;

	public Drone(Maze m) {
		this.m = m;
		counter=0;

		x=(int)(Math.random()*(Settings.width/Settings.tileWidth));
		y=(int)(Math.random()*(Settings.width/Settings.tileWidth));

		xp = x*Settings.tileWidth;
		yp = y*Settings.tileWidth;
		halfTile = Settings.tileWidth/2;
	}

	public void move() {
		ArrayList<int[]> moves = new ArrayList<int[]>();
		if(!m.getTile(x, y).hasWall(2)) {
			moves.add(new int[]{x,y+1});
		}
		if(!m.getTile(x, y).hasWall(1)) {
			moves.add(new int[]{x-1,y});
		}
		if(!m.getTile(x, y).hasWall(0)) {
			moves.add(new int[]{x,y-1});
		}
		if(!m.getTile(x, y).hasWall(3)) {
			moves.add(new int[]{x+1,y});
		}

		int[] move = moves.get((int)(Math.random()*moves.size()));

		x=move[0];
		y=move[1];
	}

	public void tick() {
		if(counter++ >20) {
			move();
			counter=0;
		}

		xp = x*Settings.tileWidth;
		yp = y*Settings.tileWidth;
	}

	public void render() {
		StdDraw.setPenColor(Color.RED);
		StdDraw.filledSquare(xp, yp, halfTile-(halfTile/5));
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}
}

