package Entities;

import java.awt.Color;
import java.util.ArrayList;

import dev.Maze;
import dev.Settings;
import dev.gfx.StdDraw;
import projectiles.Bullet;
public class Runner {
	
	private Maze m;
	
	private int x, y;
	private float xp, yp, halfTile;
	private int direction;
	private ArrayList<Bullet> bbs;
	
	public Runner(Maze m) {
		bbs = new ArrayList<Bullet>();
		this.m = m;
		
		x=0;
		y=0;
		
		xp = x*Settings.tileWidth;
		yp = y*Settings.tileWidth;
		halfTile = Settings.tileWidth/2;
	}
	
	public void shoot() {
		if(direction == 1) {
			bbs.add(new Bullet(new float[] {xp,yp}, 0 , Settings.tileWidth/2, m));
		} else if(direction == 2) {
			bbs.add(new Bullet(new float[] {xp,yp}, -Settings.tileWidth/2 , 0, m));
		} else if(direction == 3) {
			bbs.add(new Bullet(new float[] {xp,yp}, 0 , -Settings.tileWidth/2, m));
		} else if(direction == 4) {
			bbs.add(new Bullet(new float[] {xp,yp}, Settings.tileWidth/2 , 0, m));
		}
	}
	
	public ArrayList<Bullet> getBbs() {
		return bbs;
	}

	public void setBbs(ArrayList<Bullet> bbs) {
		this.bbs = bbs;
	}

	/*
	 * directions
	 * 
	 * 1== up
	 * 2==left
	 * 3==down
	 * 4==right
	 */
	public void move(int dir) {
		if(dir == 1) {
			if(!m.getTile(x, y).hasWall(2)) {
				y+=1;
				direction=1;
			}
		} else if(dir == 2) {
			if(!m.getTile(x, y).hasWall(1)) {
				x-=1;
				direction=2;
			}
		} else if(dir == 3) {
			if(!m.getTile(x, y).hasWall(0)) {
				y-=1;
				direction=3;
			}
		} else if(dir == 4) {
			if(!m.getTile(x, y).hasWall(3)) {
				x+=1;
				direction=4;
			}
		}
	}
	
	public void tick() {
		xp = x*Settings.tileWidth;
		yp = y*Settings.tileWidth;
	}
	
	public void render() {
		StdDraw.setPenColor(new Color(255,255,255));
		StdDraw.filledSquare(xp, yp, halfTile-halfTile/5);
	}
	public int getX() {
		return x;
	}
	public int getY() {
		return y;
	}
}
