package projectiles;

import java.awt.Color;

import dev.Maze;
import dev.Settings;
import dev.gfx.StdDraw;

public class Bullet {

	private float[] pos;
	private float[] velocity;
	private Maze m;


	public Bullet(float[] pos ,int x, int y, Maze m) {
		this.m = m;
		this.pos=pos;
		velocity = new float[] {x,y};
	}

	public void tick() {
		this.pos = Physics.Physics.addVector(pos, velocity);
	}

	public void render() {
		StdDraw.setPenColor(Color.ORANGE);
		if(velocity[0]!=0) {
			StdDraw.filledRectangle(pos[0], pos[1], 
					Settings.tileWidth/2, Settings.tileWidth/4);
		} else {
			StdDraw.filledRectangle(pos[0], pos[1], 
					Settings.tileWidth/4, Settings.tileWidth/2);
		}
	}
	
	public boolean outOfBouds() {
		if(pos[0]>Settings.width) {
			return true;
		} else if(pos[1]>Settings.height) {
			return true;
		}
		return false;
	}

	public int getX() {
		return ((int)(pos[0]/Settings.tileWidth));
	}
	public int getY() {
		return ((int)(pos[1]/Settings.tileWidth));
	}
}
